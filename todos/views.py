from django.shortcuts import render, get_object_or_404


# Create your views here.
def todo_list_list(request):
    task_list = TodoList.objects.all()
    context = {
        "todo_list": task_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {"item_object": detail}
    return render(request, "todos/detail.html", context)
